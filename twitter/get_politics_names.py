# importing the requests library 
import requests 
import itertools
import json
  
# api-endpoint 
URL = "http://192.168.11.126:5000/queryData"
  
  
# defining a params dict for the parameters to be sent to the API 
PARAMS_PERSONS = [{"days":1, "entityType":"person"}]
PARAMS_PARTYS = [{"days":1, "entityType":"party"}]
  
# sending get request and saving the response as response object 
request_person = requests.post(url = URL, json = PARAMS_PERSONS) 
request_partys = requests.post(url = URL, json = PARAMS_PARTYS) 
  
# extracting data in json format 
data_person = request_person.json()
data_partys = request_partys.json()

#extract persons names
persons_names = [person['Name'] for person in data_person]
#persons_names = list(itertools.chain.from_iterable(persons_names))

#extract partys names
partys_names = [party['Name'] for party in data_partys]
#partys_names = list(itertools.chain.from_iterable(partys_names))

#save to file
with open('person_names.json', 'w') as f:  
    json.dump(persons_names, f)
    f.close()

with open('partys_names.json', 'w') as f:  
    json.dump(partys_names, f)
    f.close()

