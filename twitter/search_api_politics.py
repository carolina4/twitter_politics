# Import the Twython class
from twython import Twython  
import json
import pandas as pd
import pymongo
from datetime import datetime

# Twitter ###############################################################
# Load credentials from json file
with open("twitter_credentials.json", "r") as file:  
    creds = json.load(file)

# Instantiate an object
python_tweets = Twython(creds['CONSUMER_KEY'], creds['CONSUMER_SECRET'])

# Mongo DB ##############################################################
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["twitter_politics2"]
politics_col = mydb["politics"]
partys_col = mydb["partys"]

# Query Twitter persons ##################################################
with open('person_names.json') as json_file:  
    persons_names = json.load(json_file)
    for person_list in persons_names:
        for person in person_list:
            query = {'q': person,  
                'count': 100,
            }
            # Search tweets  
            for status in python_tweets.search(**query)['statuses']:  

                tweet_dict = {'search_keyword': person_list, 
                    'coordinates':[], 
                    'username': [], 
                    'followers_count': [], 
                    'retweet_count': [], 
                    'text': [], 
                    'created_at': [],
                    'hashtags': [],
                    'lang': [],
                    'tweet_id': '',
                    'date': ''} 
                tweet_dict['coordinates'].append(status['coordinates'])
                tweet_dict['username'].append(status['user']['name']) 
                tweet_dict['followers_count'].append(status['user']['followers_count']) 
                tweet_dict['retweet_count'].append(status['retweet_count']) 
                tweet_dict['text'].append(status['text'])
                tweet_dict['created_at'].append(status['created_at']) 
                tweet_dict['hashtags'].append(status['entities']['hashtags'])
                tweet_dict['lang'].append(status['lang'])
                tweet_dict['tweet_id'] = status['id']
                tweet_dict['date'] = datetime.today().strftime('%Y-%m-%d')
                politics_col.insert_one(tweet_dict)
    json_file.close()

# Query Twitter partys ##################################################
with open('partys_names.json') as json_file:  
    partys_names = json.load(json_file)
    for partys_list in partys_names:
        for party in partys_list:
            query = {'q': party,  
                'count': 100,
            }
            # Search tweets  
            for status in python_tweets.search(**query)['statuses']:  

                tweet_dict = {'search_keyword': partys_list, 
                    'coordinates':[], 
                    'username': [], 
                    'followers_count': [], 
                    'retweet_count': [], 
                    'text': [], 
                    'created_at': [],
                    'hashtags': [],
                    'lang': [],
                    'tweet_id': '',
                    'date': ''} 
                tweet_dict['coordinates'].append(status['coordinates'])
                tweet_dict['username'].append(status['user']['name']) 
                tweet_dict['followers_count'].append(status['user']['followers_count']) 
                tweet_dict['retweet_count'].append(status['retweet_count']) 
                tweet_dict['text'].append(status['text'])
                tweet_dict['created_at'].append(status['created_at']) 
                tweet_dict['hashtags'].append(status['entities']['hashtags'])
                tweet_dict['lang'].append(status['lang'])
                tweet_dict['tweet_id'] = status['id']
                tweet_dict['date'] = datetime.today().strftime('%Y-%m-%d')

                partys_col.insert_one(tweet_dict)
    json_file.close()

myclient.close()


# partys_col

'''
# Structure data in a pandas DataFrame for easier manipulation
df = pd.DataFrame(dict_)  
df.sort_values(by='favorite_count', inplace=True, ascending=False)  
df.head(5)

with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    print(df.loc[[0]])
'''

