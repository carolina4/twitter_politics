
from flask import Flask, render_template, send_from_directory
app = Flask(__name__)

# FLASK_APP=backend.py flask run

@app.route('/bar_chart')#, methods=['POST']
def update_bar_chart():
    #graph_data = {"graph_data": "/static/data/person_"+days_ago+".json"}
    #return render_template('horizontal_bar_chart.html', template_data=graph_data)
    return render_template('vertical_bar_chart.html')

@app.route("/icon_chart")
def render_icon_chart():
    template_data = {"data_file_name": "/static/data/data.tsv"}
    return render_template('icon_chart2.html', template_data=template_data)

@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('static', "data/"+path)