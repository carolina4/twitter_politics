from flask import Flask, render_template, send_from_directory, request, jsonify
from .get_chart_data import *

app = Flask(__name__)

# FLASK_APP=server.py flask run

@app.route('/bar_chart')
def update_bar_chart():
    return render_template('vertical_bar_chart.html', server_url="http://127.0.0.1:5000/")

@app.route("/occurrences", methods=['POST'])
def chart_data():
    entity = request.get_json()['entity']
    days_ago = request.get_json()['days_ago']
    chart_data = get_chart_data(entity, days_ago)
    return jsonify(chart_data)

@app.route("/followers", methods=['POST'])
def followers_chart_data():
    chart_data = get_followers_chart_data()
    return jsonify(chart_data)

@app.route("/retweets", methods=['POST'])
def retweet_chart_data():
    entity = request.get_json()['entity']
    days_ago = request.get_json()['days_ago']
    chart_data = get_retweet_chart_data(entity,days_ago)
    return jsonify(chart_data)

'''
@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('static', "data/"+path)
'''