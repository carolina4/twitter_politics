# Import the Twython class
from TwitterAPI import TwitterAPI
import json
import pandas as pd
import pymongo

# Twitter auth
consumer_key = 'oSnZwRtXIwKQeujTyxtEbjQBX'   
consumer_secret = 'wB1gcdmJIeT06VL4CYPJ84YijUwg2WPh4vVKfa9ShuCIBb5qzF'  
access_token_key = '1116268359635689472-J0Jbv3TqoLbrEz9BKB4pimeF73RjfM'  
access_token_secret = 'eMHMAydqUI0hFQ9nJBzVUpNQh5ljhquqzN6279qrlwxYe'

api = TwitterAPI(consumer_key, consumer_secret, access_token_key, access_token_secret)
r = api.request('https://api.twitter.com/1.1/tweets/search/30day/dev/counts.json',
                {'query': 'Francesco Vanderjeugd'})

for item in r:
    print(item)

'''
# Twitter ###############################################################
# Load credentials from json file
with open("twitter_credentials.json", "r") as file:  
    creds = json.load(file)

# Instantiate an object
python_tweets = Twython(creds['CONSUMER_KEY'], creds['CONSUMER_SECRET'])

# Mongo DB ##############################################################
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["twitter_politics"]
politics_col = mydb["politics"]
partys_col = mydb["partys"]

# Query Twitter persons ##################################################
with open('person_names.json') as json_file:  
    persons_names = json.load(json_file)
    for person_list in persons_names:
        for person in person_list:
            query = {'q': person,  
                'count': 100,
            }
            # Search tweets  
            for status in python_tweets.search(**query)['statuses']:  

                tweet_dict = {'search_keyword': person_list, 
                    'coordinates':[], 
                    'username': [], 
                    'followers_count': [], 
                    'retweet_count': [], 
                    'text': [], 
                    'created_at': [],
                    'hashtags': [],
                    'lang': []} 
                tweet_dict['coordinates'].append(status['coordinates'])
                tweet_dict['username'].append(status['user']['name']) 
                tweet_dict['followers_count'].append(status['user']['followers_count']) 
                tweet_dict['retweet_count'].append(status['retweet_count']) 
                tweet_dict['text'].append(status['text'])
                tweet_dict['created_at'].append(status['created_at']) 
                tweet_dict['hashtags'].append(status['entities']['hashtags'])
                tweet_dict['lang'].append(status['lang'])

                politics_col.insert_one(tweet_dict)
    json_file.close()

# Query Twitter partys ##################################################
with open('partys_names.json') as json_file:  
    partys_names = json.load(json_file)
    for partys_list in partys_names:
        for party in partys_list:
            query = {'q': party,  
                'count': 100,
            }
            # Search tweets  
            for status in python_tweets.search(**query)['statuses']:  

                tweet_dict = {'search_keyword': partys_list, 
                    'coordinates':[], 
                    'username': [], 
                    'followers_count': [], 
                    'retweet_count': [], 
                    'text': [], 
                    'created_at': [],
                    'hashtags': [],
                    'lang': []} 
                tweet_dict['coordinates'].append(status['coordinates'])
                tweet_dict['username'].append(status['user']['name']) 
                tweet_dict['followers_count'].append(status['user']['followers_count']) 
                tweet_dict['retweet_count'].append(status['retweet_count']) 
                tweet_dict['text'].append(status['text'])
                tweet_dict['created_at'].append(status['created_at']) 
                tweet_dict['hashtags'].append(status['entities']['hashtags'])
                tweet_dict['lang'].append(status['lang'])

                partys_col.insert_one(tweet_dict)
    json_file.close()

myclient.close()


# partys_col

'''


